import React, {useState} from 'react';
import convertBinaryToDecimal from "./utils/convertBinaryToDecimal";
import TextField from '@mui/material/TextField';
import convertDecimalToBinary from "./utils/convertDecimalToBinary";
import {makeStyles} from "@mui/styles";

import './App.css';


function App() {
    const [binaryValue, setBinaryValue] = useState<string>("");
    const [binaryValueError, setBinaryValueError] = useState<boolean>(false);

    const [decimalValue, setDecimalValue] = useState<string>("");
    const [decimalValueError, setDecimalValueError] = useState<boolean>(false);

    const updateDecimalValue = (binary: string): void => {
        setBinaryValue(binary);
        setBinaryValueError(false);
        setDecimalValueError(false);

        // Verify input binary string only contains 0s and 1s
        const binaryRegex = /^[0-1]+$/;
        if (!binaryRegex.test(binary)) {
            setDecimalValue("");

            // We do not want to show the error if the text field is empty
            if (binary.length !== 0) {
                setBinaryValueError(true);
            }
            return;
        }

        setBinaryValueError(false);

        const decimalValue = convertBinaryToDecimal(parseInt(binary));
        setDecimalValue(decimalValue.toString());
    };

    const updateBinaryValue = (decimal: string): void => {
        setDecimalValue(decimal);
        setBinaryValueError(false);
        setDecimalValueError(false);

        // Verify input only contains digits
        const numberRegex = /^\d+$/;
        if (!numberRegex.test(decimal)) {
            setBinaryValue("");

            // We do not want to show the error if the text field is empty
            if (decimal.length !== 0) {
                setDecimalValueError(true);
            }

            return;
        }

        const binaryValue = convertDecimalToBinary(parseInt(decimal));
        setBinaryValue(binaryValue);
    }

    const styles = useStyles();

    return (
        <div className={styles.container}>
            <h1 className={styles.title}>Binary To Decimal Converter</h1>
                <TextField
                    autoComplete='off'
                    className={styles.textField}
                    variant={"standard"}
                    error={binaryValueError}
                    id="outlined-error"
                    value={binaryValue}
                    placeholder={"Enter Binary Value"}
                    onChange={(event) => updateDecimalValue(event.target.value)}
                    helperText={binaryValueError ? "Please input a valid binary number. You must only include 1s and 0s" : ""}
                />
                <TextField
                    autoComplete='off'
                    className={styles.textField}
                    variant={"standard"}
                    error={decimalValueError}
                    id="outlined-error"
                    value={decimalValue}
                    placeholder={"Enter Decimal Value"}
                    onChange={(event) => updateBinaryValue(event.target.value)}
                    helperText={decimalValueError ? "Please input a decimal base 10 number." : ""}
                />
            <p className={styles.footNotes}>This converter supports converting Binary From / To Decimal</p>
        </div>
    );
}


const useStyles = makeStyles({
    container: {
        display: "flex",
        flex: 1,
        height: "100%",
        width: "100%",
        backgroundColor: "#1c1c1c",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
    title: {
        color: "white",
        fontSize: 50,
        fontWeight: 500,
        fontFamily: 'Roboto,Helvetica,Arial,sans-serif'
    },
    inputContainer: {
        height: "40%",
        width: "80%",
        display: "flex",
        flex: 1,
        justifyContent: "center"
    },
    textField: {
        display: "flex",
        borderColor: "red",
        height: "40%",
        width: "80%",
        justifyContent: "center",
        '& .MuiInput-underline:before': {
            borderBottomColor: '#5e5e5e',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: '#777777',
        },
        '& .MuiFormHelperText-root.Mui-error': {
            textAlign: "center",
            fontSize: 16,
        },
        '& .MuiInput-root': {
            color: "white"
        },
        '& .MuiInputBase-input': {
            color: "white",
            fontSize: 50,
            textAlign: "center",
        }
    },
    footNotes: {
        color: "rgba(255, 255, 255, 0.57)",
        fontSize: 12,
        fontWeight: 500,
        fontFamily: 'Roboto,Helvetica,Arial,sans-serif'
    }
});

export default App;
