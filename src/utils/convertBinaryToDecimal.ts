export default function convertBinaryToDecimal(binary: number): number {
    let exponential = 0;
    let result = 0;
    while(binary !== 0){
        let lastDigit = binary % 10;
        result += Math.pow(2, exponential) * lastDigit;

        binary = Math.trunc(binary / 10);
        exponential++;
    }

    return result;
}
