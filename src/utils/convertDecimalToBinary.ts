export default function convertDecimalToBinary(decimal: number): string {

    const binaryDigits = [];

    while (decimal > 0) {
        binaryDigits.push(decimal % 2);
        decimal = Math.floor(decimal / 2);
    }

    return binaryDigits.reverse().join("");
};

